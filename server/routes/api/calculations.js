const express = require("express");
const fs = require("fs");

const router = express.Router();

// Get Posts
router.get("/", async (req, res) => {
  const data = await loadJsonCollection();
  res.send(await data);
});

// Add Post
router.post("/", async (req, res) => {
  var jsonObj = {};
  jsonObj.jsonData = [];
  results = [];
  const rawdata = await loadJsonCollection();

  var calculations = {
    id: rawdata.jsonData.length + 1,
    firstNr: req.body.n1,
    lastNr: req.body.n2,
    results: calculateMultiples(req.body.n1, req.body.n2)
  };

  rawdata.jsonData.push(calculations);

  writeJsonCollection(rawdata);
  
  res.status(201).send();
});

// Delete Post
router.delete("/:id", async (req, res) => {
  const data = await loadJsonCollection();

  for (i = 0; i < data.jsonData.length; i++) {
    if (data.jsonData[i].id == req.params.id) {
      data.jsonData.splice(i, 1);
    }
  }

  writeJsonCollection(data)

  res.status(200).send({});
});

async function loadJsonCollection() {
  let rawdata = fs.readFileSync("output.json");
  let customers = JSON.parse(rawdata);
  return customers;
}

async function writeJsonCollection(jsonObj) {
  // stringify JSON Object
  var jsonContent = JSON.stringify(jsonObj);

  fs.writeFile("output.json", jsonContent, "utf8", function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }

    console.log("JSON file has been saved.");
  });
}


function calculateMultiples(n1, n2) {
  var arr = [];
  var n1 = parseInt(n1);
  var n2 = parseInt(n2);

  if (n1 >= 0 && n2 >= n1) {
    for (i = n1; i <= n2; i++) {
      if (i % 3 == 0 && i % 5 == 0) {
        arr.push("PéDo");
      } else if (i % 3 == 0) {
        arr.push("Pé");
      } else if (i % 5 == 0) {
        arr.push("Do");
      } else {
        arr.push(i);
      }
    }
    return arr;
  } else if (n2 >= 0 && n1 >= n2) {
    for (i = n1; i >= n2; i--) {
      if (i % 3 == 0 && i % 5 == 0) {
        arr.push("PéDo");
      } else if (i % 3 == 0) {
        arr.push("Pé");
      } else if (i % 5 == 0) {
        arr.push("Do");
      } else {
        arr.push(i);
      }
    }
    return arr;
  } else {
    return "Fora Dos Limites";
  }
}

module.exports = router;
